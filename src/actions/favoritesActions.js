export const ADD_TO_FAVORITES = 'ADD_TO_FAVORITES';
export const REMOVE_FROM_FAVORITES = 'REMOVE_FROM_FAVORITES';
export const SET_FAVORITES = 'SET_FAVORITES';

// Action Creators
export const addToFavorites = (product) => {
  return { type: ADD_TO_FAVORITES, product };
};

export const removeFromFavorites = (sku) => {
  return { type: REMOVE_FROM_FAVORITES, sku };
};

export const setFavorites = (favorites) => {
  return { type: SET_FAVORITES, favorites };
};
