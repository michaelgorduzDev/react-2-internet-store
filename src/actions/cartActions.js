export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const SET_CART = 'SET_CART';

// Action creators
export const addToCart = (product) => ({
  type: ADD_TO_CART,
  product,
});

export const removeFromCart = (sku) => ({
  type: REMOVE_FROM_CART,
  sku,
});

export const setCart = (cart) => ({
  type: SET_CART,
  cart,
});
