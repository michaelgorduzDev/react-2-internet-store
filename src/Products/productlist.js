import './product-list.scss';
import React from 'react';

function ProductList({ products, addToCart, addToFavorites, favorites, removeFromFavorites }) {
  // Create a Set containing the SKUs of products in favorites
  const favoriteProductSKUs = new Set(favorites.map((product) => product.sku));

  const toggleFavorite = (product) => {
    if (favoriteProductSKUs.has(product.sku)) {
      // If the product is already in favorites, remove it
      removeFromFavorites(product.sku);
    } else {
      // Otherwise, add it to favorites
      addToFavorites(product);
    }
  };

  return (
    <div className="product-list">
      <ul>
        {products.map((product) => (
          <li key={product.sku}>
            <h2>{product.name}</h2>
            <p>Price: ${product.price}</p>
            <img src={product.image} alt={product.name} />
            <button onClick={() => addToCart(product)}>Add to Cart</button>
            <button
              className={favoriteProductSKUs.has(product.sku) ? 'favorite-star active' : 'favorite-star'}
              onClick={() => toggleFavorite(product)}
            >
              {favoriteProductSKUs.has(product.sku) ? '★' : '☆'}
            </button>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default ProductList;
