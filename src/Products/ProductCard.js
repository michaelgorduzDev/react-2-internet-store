import React from 'react';

function ProductCard({ product, onRemove }) {
  return (
    <div className="product-card">
      <img src={product.image} alt={product.name} className="modal-item-image" />
      <span>{product.name}: ${product.price} (Quantity: {product.quantity})</span>
      <button onClick={() => onRemove(product.sku)}>Remove</button>
    </div>
  );
}

export default ProductCard;
