import React, { useEffect, useState } from 'react';
import './CartPage.scss';
import Modal from '../Modal/modal';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';

function CartPage({ cart, removeFromCart }) {
  const [modalOpen, setModalOpen] = useState(false);
  const [itemToRemove, setItemToRemove] = useState(null);
  const [cartCleared, setCartCleared] = useState(false); // Track whether the cart is cleared

  useEffect(() => {
    // Check if the cart has been cleared and close the modal if it has
    if (cartCleared) {
      closeModal();
      setCartCleared(false); // Reset the cartCleared state
    }
  }, [cartCleared]);

  const openModal = (item) => {
    setItemToRemove(item);
    setModalOpen(true);
  };

  const closeModal = () => {
    setItemToRemove(null);
    setModalOpen(false);
  };

  // Function to clear the cart in localStorage
  const clearCart = () => {
    localStorage.removeItem('cart');
  };

  const confirmRemove = () => {
    if (itemToRemove) {
      removeFromCart(itemToRemove.sku);
    }
    closeModal();
  };

  // Валідація за допомогою Yup
  const validationSchema = Yup.object().shape({
    firstName: Yup.string().required('Enter first name'),
    lastName: Yup.string().required('Enter last name'),
    age: Yup.number().required('Enter age').positive().integer(),
    address: Yup.string().required('Enter address'),
    phoneNumber: Yup.string().required('Enter phone number'),
  });

  // Функція для обробки поданої форми
  const handleCheckout = (values) => {
    // Clearing localStorage.
    clearCart();
    // Set cartCleared to true to trigger the useEffect
    setCartCleared(true);
    console.log('Придбані товари:', cart);
    console.log('Дані з форми:', values);
  
    // Reload the page after a short delay 
    setTimeout(() => {
      window.location.reload();
    }, 5000);
  };  

  return (
    <div className='cart-wrapper'>
      <h2>Cart checkout</h2>
      <ul>
        {cart.map((item) => (
          <li key={item.sku}>
            <img src={item.image} alt={item.name} className="cart-item-image" />
            {item.name} (Quantity: {item.quantity}) -&nbsp; <span className='cart-item-price'> ${item.price}</span>
            <button className='remove-btn' onClick={() => openModal(item)}>X</button>
          </li>
        ))}
      </ul>
      {modalOpen && (
        <Modal
          header="Confirm Removal"
          text="Are you sure you want to remove this item from your cart?"
          actions={
            <div>
              <button className='confirm-button' onClick={confirmRemove}>Yes</button>
              <button className='confirm-button' onClick={closeModal}>No</button>
            </div>
          }
          onClose={closeModal}
          closeButton={true}
          isOpen={modalOpen}
        />
      )}

      {/* Форма для інформації про кошик і адресу доставки */}
      <Formik
        initialValues={{
          firstName: '',
          lastName: '',
          age: '',
          address: '',
          phoneNumber: '',
        }}
        validationSchema={validationSchema}
        onSubmit={handleCheckout}
      >
        <Form>
          <div className="form-field">
            <label htmlFor="firstName">Ім'я користувача:</label>
            <Field type="text" name="firstName" />
            <ErrorMessage name="firstName" component="div" className="error-message" />
          </div>

          <div className="form-field">
            <label htmlFor="lastName">Прізвище користувача:</label>
            <Field type="text" name="lastName" />
            <ErrorMessage name="lastName" component="div" className="error-message" />
          </div>

          <div className="form-field">
            <label htmlFor="age">Вік користувача:</label>
            <Field type="number" name="age" />
            <ErrorMessage name="age" component="div" className="error-message" />
          </div>

          <div className="form-field">
            <label htmlFor="address">Адреса доставки:</label>
            <Field type="text" name="address" />
            <ErrorMessage name="address" component="div" className="error-message" />
          </div>

          <div className="form-field">
            <label htmlFor="phoneNumber">Мобільний телефон:</label>
            <Field type="text" name="phoneNumber" />
            <ErrorMessage name="phoneNumber" component="div" className="error-message" />
          </div>

          <button type="submit" className="checkout-button">
            Checkout
          </button>
        </Form>
      </Formik>
    </div>
  );
}

export default CartPage;
