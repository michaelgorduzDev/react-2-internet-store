import React, { useState, useEffect } from 'react';
import Button from './Button/button';
import Modal from './Modal/modal';
import ProductList from './Products/productlist';
import productsData from './products.json';
import ProductCard from './Products/ProductCard';
import Navigation from './Navigation/Navigation';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import './App.scss';

import CartPage from './CartPage/CartPage';
import FavoritesPage from './FavoritesPage/FavoritesPage';

function App() {
  const [cart, setCart] = useState(() => {
    const cartData = localStorage.getItem('cart');
    return cartData ? JSON.parse(cartData) : [];
  });

  const [favorites, setFavorites] = useState(() => {
    const favoritesData = localStorage.getItem('favorites');
    return favoritesData ? JSON.parse(favoritesData) : [];
  });

  const [showCartModal, setShowCartModal] = useState(false);
  const [showFavoritesModal, setShowFavoritesModal] = useState(false);

  const itemsInCart = cart.length > 0;
  const itemsInFavorites = favorites.length > 0;

  useEffect(() => {
    localStorage.setItem('cart', JSON.stringify(cart));
  }, [cart]);

  useEffect(() => {
    localStorage.setItem('favorites', JSON.stringify(favorites));
  }, [favorites]);

  const addToCart = (product) => {
    const itemIndex = cart.findIndex((item) => item.sku === product.sku);
    if (itemIndex !== -1) {
      const updatedCart = [...cart];
      updatedCart[itemIndex].quantity += 1;
      setCart(updatedCart);
    } else {
      setCart([...cart, { ...product, quantity: 1 }]);
    }

    //   // Show the cart confirmation modal
    showCartConfirmationModal(product.name);
  };

  const showCartConfirmationModal = (itemName) => {
    setShowCartModal(true);
  };

  const addToFavorites = (product) => {
    setFavorites([...favorites, product]);
  };

  const removeFromFavorites = (sku) => {
    const updatedFavorites = favorites.filter((item) => item.sku !== sku);
    setFavorites(updatedFavorites);
  };

  const openCartModal = () => {
    setShowCartModal(true);
  };

  const closeCartModal = () => {
    setShowCartModal(false);
  };

  const openFavoritesModal = () => {
    setShowFavoritesModal(true);
  };

  const closeFavoritesModal = () => {
    setShowFavoritesModal(false);
  };

  const removeFromCart = (sku) => {
    const itemIndex = cart.findIndex((item) => item.sku === sku);
    if (itemIndex !== -1) {
      const updatedCart = [...cart];
      if (updatedCart[itemIndex].quantity > 1) {
        updatedCart[itemIndex].quantity -= 1;
      } else {
        updatedCart.splice(itemIndex, 1);
      }
      setCart(updatedCart);
    }
  };


  const totalQuantity = cart.reduce((accumulator, item) => accumulator + item.quantity, 0);

  return (
    <Router>
      <div className="App">
      <header>
          <Navigation /> {/* компонент навігації */}
        </header>
        <Routes>
          <Route path="/" element={<ProductList products={productsData} addToCart={addToCart} addToFavorites={addToFavorites} favorites={favorites} removeFromFavorites={removeFromFavorites} />} />
          <Route path="/cart" element={<CartPage cart={cart} removeFromCart={removeFromCart} />} />
          <Route path="/favorites" element={<FavoritesPage favorites={favorites} removeFromFavorites={removeFromFavorites} />} />
        </Routes>
        <Button
          className={itemsInCart ? 'cart itemsInCart' : 'cart'}
          text={`View Cart (${totalQuantity})`} // Use the totalQuantity
          onClick={openCartModal}
        />
        <Button
          className={itemsInFavorites ? 'favorites itemsInFavorites' : 'favorites'}
          text={`Favorites (${favorites.length})`}
          onClick={openFavoritesModal}
        />

        {showCartModal && (
          <Modal
            header="Shopping Cart"
            closeButton={true}
            text={`Items in Cart (${totalQuantity})`}
            onClose={closeCartModal}
          >
            {cart.length > 0 ? (
              <ul>
                <li>
                  <p className='last-added' style={{ color: 'green' }}>Last added:</p>
                </li>
                {cart.map((item, index) => {
                  const isLastAddedItem = index === cart.length - 1;

                  return (
                    <li
                      key={item.sku}
                      style={{
                        backgroundColor: isLastAddedItem ? 'lightgreen' : 'white',
                      }}
                    >
                      {isLastAddedItem && (
                        <p style={{ color: 'green' }}></p>
                      )}
                      <ProductCard product={item} onRemove={removeFromCart} />
                    </li>
                  );
                }).reverse()} {/* Reverse the order of elements to move the last added item to the top */}
              </ul>
            ) : (
              <p>Your cart is empty.</p>
            )}
          </Modal>
        )}

        {showFavoritesModal && (
          <Modal
            header="Favorites"
            closeButton={true}
            text={`Favorites (${favorites.length})`}
            onClose={closeFavoritesModal}
          >
            <ul>
              {favorites.map((item) => (
                <li key={item.sku}>
                  <img src={item.image} alt={item.name} className="modal-item-image" />
                  <span>{item.name}: ${item.price}</span>
                  <button onClick={() => removeFromFavorites(item.sku)}>Remove</button>
                </li>
              ))}
            </ul>
          </Modal>
        )}
      </div>
    </Router>
  );
}

export default App;
