import React from 'react';
// import './Favorites.scss';

function FavoritesPage({ favorites, removeFromFavorites }) {
  return (
    <div className='cart-wrapper'>
      <h2>Favorites</h2>
      <ul>
        {favorites.map((item) => (
         <li key={item.sku}>
         <img src={item.image} alt={item.name} className="cart-item-image" />
         {item.name} (Quantity: {item.quantity}) -&nbsp; <span className='cart-item-price'> ${item.price}</span>
            <button onClick={() => removeFromFavorites(item.sku)}>★</button>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default FavoritesPage;
