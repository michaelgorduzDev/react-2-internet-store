import React from 'react';
import { NavLink } from 'react-router-dom';
import './Navigation.scss';

function Navigation() {
  return (
    <div>
      <ul className="links">
        <li>
          <NavLink to="/">HOME</NavLink>
        </li>
        <li>
          <NavLink to="/cart">CART</NavLink>
        </li>
        <li>
          <NavLink to="/favorites">FAVORITES</NavLink>
        </li>
      </ul>
    </div>
  );
}

export default Navigation;
