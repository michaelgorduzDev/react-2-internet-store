import React from 'react';
import './Button.scss';

const Button = ({ className, backgroundColor, text, onClick }) => {
  // Use the specified className along with 'custom-button'
  const buttonClassName = `custom-button ${className || ''}`;

  return (
    <button className={buttonClassName} style={{ backgroundColor }} onClick={onClick}>
      {text}
    </button>
  );
};

export default Button;
