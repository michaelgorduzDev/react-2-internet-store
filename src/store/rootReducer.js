import { combineReducers } from 'redux';
import cartReducer from './cartReducer';
import favoritesReducer from './favoritesReducer';

const rootReducer = combineReducers({
  cart: cartReducer,
  favorites: favoritesReducer,
  // Add more reducers here 
});

export default rootReducer;