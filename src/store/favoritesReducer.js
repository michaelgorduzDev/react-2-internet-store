const initialState = [];

const favoritesReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_TO_FAVORITES':
      // Handle adding to favorites logic
      const productToAdd = action.product;
      const existingProduct = state.find(item => item.sku === productToAdd.sku);

      if (!existingProduct) {
        // If the product is not already in favorites, add it
        return [...state, productToAdd];
      }
      // If the product is already in favorites, return the current state
      return state;

    case 'REMOVE_FROM_FAVORITES':
      // Handle removing from favorites logic
      const skuToRemove = action.sku;
      const updatedFavorites = state.filter(item => item.sku !== skuToRemove);
      return updatedFavorites;

      case 'SET_FAVORITES':
        // Set the favorites to the provided favorites data
        return action.favorites;

    default:
      return state;
  }
};

export default favoritesReducer;
