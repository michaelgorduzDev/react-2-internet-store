const initialState = [];

const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_TO_CART':
      // Handle adding to cart logic
      const productToAdd = action.product;
      const existingProductIndex = state.findIndex(item => item.sku === productToAdd.sku);

      if (existingProductIndex !== -1) {
        // If the product already exists in the cart, update its quantity
        const updatedState = [...state];
        updatedState[existingProductIndex].quantity += 1;
        return updatedState;
      } else {
        // If the product is not in the cart, add it with a quantity of 1
        return [...state, { ...productToAdd, quantity: 1 }];
      }

    case 'REMOVE_FROM_CART':
      // Handle removing from cart logic
      const skuToRemove = action.sku;
      const updatedCart = state.filter(item => item.sku !== skuToRemove);
      return updatedCart;

      case 'SET_CART':
        // Set the cart to the provided cart data
        return action.cart;

    default:
      return state;
  }
};

export default cartReducer;
